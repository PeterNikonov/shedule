<?php

namespace PeterNikonov\Shedule\Resolver;

use DateInterval;
use DatePeriod;
use DateTime;
use PeterNikonov\Shedule\Shedule;
use function array_filter;
use function array_keys;
use function array_merge;
use function is_null;

/**
 * Class DateSheduleResolver
 * @package Shedule
 *
 * Определяет приоритет графика для использования
 */
class ShedulesResolver
{
    /**
     * @var DateTime
     */
    protected $date;
    /**
     * @var Shedule[]
     */
    protected $shedules = [];

    public function __construct(DateTime $date)
    {
        $this->date = $date;
    }

    public function addShedule(Shedule $shedule)
    {
        $this->shedules[] = $shedule;
    }

    /**
     * Проверить наличие и корректность периода действия графика.
     *
     * @param Shedule $shedule
     * @return bool
     */
    protected function hasPeriod(Shedule $shedule) {

        if (is_null($shedule->getStartDate()) OR is_null($shedule->getEndDate())) {
            return false;
        }

        if($shedule->getStartDate() > $shedule->getEndDate()) {
            return false;
        }

        return true;
    }

    /**
     * Проверить наличие установленные даты.
     *
     * @param Shedule $shedule
     * @return bool
     */
    protected function hasDates(Shedule $shedule) {
        return !empty($shedule->getDates());
    }

    /**
     * Проверить установленные дни недели.
     *
     * @param Shedule $shedule
     * @return bool
     */
    protected function hasWeekdays(Shedule $shedule) {
        return !empty($shedule->getWeekdays());
    }

    /**
     * Проверить, входит ли анализируемая дата в период действия графика.
     *
     * @param Shedule $shedule
     * @return bool
     */
    protected function checkDateInPeriod(Shedule $shedule) {

        $interval = new DateInterval('P1D');
        $shedulePeriod = new DatePeriod($shedule->getStartDate(), $interval, $shedule->getEndDate());

        foreach ($shedulePeriod as $point) {
            if($this->date == $point) {
                return true;
            }
        }

        return false;
    }

    /**
     * Проверить, является ли дата днем недели для которого задан график
     *
     * @param Shedule $shedule
     * @return bool
     */
    protected function checkDateWeekday(Shedule $shedule) {
        return in_array($this->date->format('l'), $shedule->getWeekdays());
    }

    /**
     * Проверить, входит ли анализируемая дата в список установленных дат для графика.
     *
     * @param Shedule $shedule
     */
    protected function checkDateInDates(Shedule $shedule) {
        return in_array($this->date->format('Y-m-d'), $shedule->getDates());
    }

    /**
     * Определить приоритет для использования
     *
     * Обычный - Если расписание составлено для дня недели, имеет бессрочный график и не содержит исключений по дате
     * Повышенный - Если установлены конкретные даты и\или если задан срок действия графика
     *
     * @param Shedule $shedule
     * @return int
     */
    protected function countPriority(Shedule $shedule) {

        $priority = 0;

        if (!$this->hasDates($shedule)) {
            $priority--;
        }

        if (!$this->hasPeriod($shedule)) {
            $priority--;
        }

        return $priority;
    }

    /**
     * Вернуть последний добавленный график с самым высоким приоритетом,
     * определяемым по признакам наличия данных в исследуемых объектах
     *
     * @return Shedule|null
     */
    public function resolve() {

        $byWeekday = array_filter($this->shedules, [$this, 'hasWeekdays']);
        $byWeekday = array_filter($byWeekday, [$this, 'checkDateWeekday']);

        $byDates = array_filter($this->shedules, [$this, 'hasDates']);
        $byDates = array_filter($byDates, [$this, 'checkDateInDates']);

        $byPeriod = array_filter($this->shedules, [$this, 'hasPeriod']);
        $byPeriod = array_filter($byPeriod, [$this, 'checkDateInPeriod']);

        $shedules = array_merge($byWeekday, $byDates, $byPeriod);

        foreach ($shedules as $k => $shedule) {
            $priority = $this->countPriority($shedule);
            $result[$priority] = $shedule;
        }

        $max = max(array_keys($result));

        return $result[$max];
    }
}
