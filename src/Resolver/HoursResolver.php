<?php

namespace PeterNikonov\Shedule\Resolver;

use DateInterval;
use DateTime;
use PeterNikonov\Shedule\Shedule;
use function sprintf;

class HoursResolver
{
    /**
     * @var Shedule $shedule
     */
    protected $shedule;
    /**
     * @var DateTime $date
     */
    protected $date;

    /**
     * HoursSheduleResolver constructor.
     * @param Shedule $shedule
     * @param DateTime $date
     */
    public function __construct(Shedule $shedule, DateTime $date)
    {
        $this->shedule = $shedule;
        $this->date = $date;
    }

    /**
     * Определить используемый раздел часов, общие, на день недели, или для даты
     */
    protected function getSection()
    {
        $start = $this->shedule->getStartTimes();

        $all = '*';
        $weekday = $this->date->format('l');
        $date = $this->date->format('Y-m-d');

        // как то красивее реализовать
        if(isset($start[$date])) return $date;
        if(isset($start[$weekday])) return $weekday;
        if(isset($start[$all])) return $all;
    }

    /**
     * @param $time
     * @return DateTime
     */
    protected function getDateTime($time) : DateTime
    {
        $spec = sprintf('%s %s', $this->date->format('Y-m-d'), $time);
        return new DateTime($spec);
    }

    protected function getValueFrom($array)
    {
        $section = $this->getSection();
        return $array[$section];
    }

    /**
     * @return DateTime
     */
    public function getStartTime() : DateTime
    {
        return $this->getDateTime($this->getValueFrom($this->shedule->getStartTimes()));
    }

    /**
     * @return DateTime
     */
    public function getEndTime() : DateTime
    {
        return $this->getDateTime($this->getValueFrom($this->shedule->getEndTimes()));
    }

    /**
     * @return DateInterval
     */
    public function getInterval() : DateInterval
    {
        $duration = $this->getValueFrom($this->shedule->getDurations());
        $intervalSpec = sprintf("PT%dM", $duration);

        return new DateInterval($intervalSpec);
    }
}
