<?php

namespace PeterNikonov\Shedule;

use DateTime;

class Shedule
{
    protected $doctor = 0;

    protected $profile = 0;

    protected $division = 0;

    /**
     * @var DateTime
     */
    protected $startDate;
    /**
     * @var DateTime
     */
    protected $endDate;

    protected $weekdays   = [];

    protected $dates      = [];

    protected $startTimes = [];

    protected $endTimes   = [];

    protected $durations  = [];

    protected $notifies   = [];

    /**
     * @return int
     */
    public function getDoctor(): int
    {
        return $this->doctor;
    }

    /**
     * @param int $doctor
     * @return Shedule
     */
    public function setDoctor(int $doctor): Shedule
    {
        $this->doctor = $doctor;
        return $this;
    }

    /**
     * @return int
     */
    public function getProfile(): int
    {
        return $this->profile;
    }

    /**
     * @param int $profile
     * @return Shedule
     */
    public function setProfile(int $profile) : Shedule
    {
        $this->profile = $profile;
        return $this;
    }

    /**
     * @return int
     */
    public function getDivision(): int
    {
        return $this->division;
    }

    /**
     * @param int $division
     * @return Shedule
     */
    public function setDivision(int $division): Shedule
    {
        $this->division = $division;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param DateTime $startDate
     * @return Shedule
     */
    public function setStartDate(DateTime $startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param DateTime $endDate
     * @return Shedule
     */
    public function setEndDate(DateTime $endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return array
     */
    public function getWeekdays(): array
    {
        return $this->weekdays;
    }

    /**
     * @param array $weekdays
     * @return Shedule
     */
    public function setWeekdays(array $weekdays): Shedule
    {
        $this->weekdays = $weekdays;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * @param array $dates
     * @return Shedule
     */
    public function setDates(array $dates): Shedule
    {
        $this->dates = $dates;
        return $this;
    }

    /**
     * @return array
     */
    public function getStartTimes(): array
    {
        return $this->startTimes;
    }

    /**
     * @param array $startTimes
     * @return Shedule
     */
    public function setStartTimes(array $startTimes): Shedule
    {
        $this->startTimes = $startTimes;
        return $this;
    }

    /**
     * @return array
     */
    public function getEndTimes(): array
    {
        return $this->endTimes;
    }

    /**
     * @param array $endTimes
     * @return Shedule
     */
    public function setEndTimes(array $endTimes): Shedule
    {
        $this->endTimes = $endTimes;
        return $this;
    }

    /**
     * @return array
     */
    public function getDurations(): array
    {
        return $this->durations;
    }

    /**
     * @param array $durations
     * @return Shedule
     */
    public function setDurations(array $durations): Shedule
    {
        $this->durations = $durations;
        return $this;
    }

    /**
     * @return array
     */
    public function getNotifies(): array
    {
        return $this->notifies;
    }

    /**
     * @param array $notifies
     * @return Shedule
     */
    public function setNotifies(array $notifies): Shedule
    {
        $this->notifies = $notifies;
        return $this;
    }
}
