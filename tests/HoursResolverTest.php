<?php

use PeterNikonov\Shedule\Resolver\HoursResolver;
use PeterNikonov\Shedule\Shedule;
use PHPUnit\Framework\TestCase;

class HoursResolverTest extends TestCase
{
    public function testHoursResolver()
    {
        $startTimes = [
            '*' => '10:30',
            'Wednesday' => '15:00',
            '2018-04-24' => '08:00',
        ];

        $endTimes = [
            '*' => '20:30',
            'Wednesday' => '22:00',
            '2018-04-24' => '12:00',
        ];

        $durations = [
            '*' => '30',
            'Wednesday' => '15',
            '2018-04-24' => '5',
        ];

        $shedule = new Shedule;
        $shedule->setStartTimes($startTimes);
        $shedule->setEndTimes($endTimes);
        $shedule->setDurations($durations);

        // check resolve for *
        $resolved = new HoursResolver($shedule, new DateTime('2018-04-20'));
        $this->assertEquals('10:30',  $resolved->getStartTime()->format('H:i'));
        $this->assertEquals('20:30',  $resolved->getEndTime()->format('H:i'));
        $this->assertEquals('30', $resolved->getInterval()->i);

        // check resolve for Wednesday
        $resolved = new HoursResolver($shedule, new DateTime('2018-04-18'));
        $this->assertEquals('15:00',  $resolved->getStartTime()->format('H:i'));
        $this->assertEquals('22:00',  $resolved->getEndTime()->format('H:i'));
        $this->assertEquals('15', $resolved->getInterval()->i);

        // check resolve for 2018-04-24
        $resolved = new HoursResolver($shedule, new DateTime('2018-04-24'));
        $this->assertEquals('08:00',  $resolved->getStartTime()->format('H:i'));
        $this->assertEquals('12:00',  $resolved->getEndTime()->format('H:i'));
        $this->assertEquals('5',  $resolved->getInterval()->i);
    }
}
