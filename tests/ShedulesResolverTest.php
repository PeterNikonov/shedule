<?php

use PeterNikonov\Shedule\Resolver\ShedulesResolver;
use PeterNikonov\Shedule\Shedule;
use PHPUnit\Framework\TestCase;

class ShedulesResolverTest extends TestCase
{
    public function testSheduleResolver()
    {
        $today = new DateTime('2018-04-18');

        $sheduleResolver = new ShedulesResolver($today);

        $sheduleA = new Shedule;
        $sheduleA->setStartDate(new DateTime('2018-04-10'));
        $sheduleA->setEndDate(new DateTime('2018-05-01'));

        $sheduleResolver->addShedule($sheduleA);

        $sheduleB = new Shedule;
        $sheduleB->setWeekdays(['Wednesday']);
        $sheduleB->setStartDate(new DateTime('2018-04-10'));
        $sheduleB->setEndDate(new DateTime('2018-05-01'));

        $sheduleResolver->addShedule($sheduleB);

        $this->assertEquals($sheduleResolver->resolve(), $sheduleB);
    }
}
